#!/bin/sh
echo "" > issues.txt
glab issue list -R gitlab-org/gitlab --author mhenriksen --label Category:SAST --label SAST::Ruleset --per-page 100 --page 1 | grep -v Showing >> issues.txt
glab issue list -R gitlab-org/gitlab --author mhenriksen --label Category:SAST --label SAST::Ruleset --per-page 100 --page 2 | grep -v Showing >> issues.txt
