## Semgrep Issue Matcher

Matches issues from [this issue list][list] to their corresponding
priority labels.

Note that the code is very hacked together.

[list]: https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Category%3ASAST&label_name%5B%5D=SAST%3A%3ARuleset&author_username=mhenriksen&first_page_size=100

## How to use

Requires [glab](https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/)
and Go (see .tool-versions for mise/asdf version).

1. Run `./fetch-issues.sh` to download an up-to-date list to `issues.txt`
1. Run `go run ./cmd/generate-old`
1. See `semgrep-rules.csv` for a summary
1. Run `sh set-priorities.sh` if you want to apply the priority labels
   - **Note:** the script will exclude issues that already have a priority
     label set.
