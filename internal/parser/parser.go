package parser

import (
	"io"

	"gopkg.in/yaml.v3"
)

func ParseRuleDef(reader io.Reader) (def RuleDef, err error) {
	err = yaml.NewDecoder(reader).Decode(&def)
	return
}

type RuleDef struct {
	Rules []struct {
		ID       string `yaml:"id"`
		Message  string `yaml:"message"`
		Severity string `yaml:"severity"`
	} `yaml:"rules"`
}

func (self RuleDef) Severity() string {
	return self.Rules[0].Severity
}

func (self RuleDef) ToPriorityLabel() string {
	switch self.Severity() {
	case "ERROR":
		return `SAST::Ruleset::P1`
	case "WARNING":
		return `SAST::Ruleset::P2`
	case "INFO":
		return `SAST::Ruleset::P3`
	}
	return ""

}
