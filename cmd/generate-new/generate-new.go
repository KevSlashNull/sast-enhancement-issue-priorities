package main

import (
	"fmt"
	"os"
	"os/exec"
	"semgrep-enhance-rules/internal/parser"
	"strings"

	"github.com/fatih/color"
	"github.com/xanzy/go-gitlab"
)

var faint *color.Color
var prefix string

func init() {
	color.NoColor = false
	faint = color.New(color.Faint)
	prefix = faint.Sprint("[") + color.YellowString("generate-new") + faint.Sprint("]")
}

var existingRules = listExistingRules()

func main() {
	client, err := gitlab.NewClient("")

	if err != nil {
		fmt.Println(prefix, err)
		os.Exit(1)
	}

	issues := fetchAllIssues(client)
	commands := []string{}

	color.Green("Issue list:")

	for issue := range issues {
		if !strings.Contains(issue.Title, ".yml") {
			continue
		}

		rule := matchIssueToRule(issue)
		if rule == nil {
			continue
		}

		commands = append(commands, fmt.Sprintf("glab issue update %d --label %s -R gitlab-org/gitlab", issue.IID, rule.ToPriorityLabel()))
		severity := rule.Severity()
		switch severity {
		case "ERROR":
			severity = color.RedString("ERROR")
		case "WARNING":
			severity = color.YellowString("WARNING")
		case "INFO":
			severity = color.CyanString("INFO")
		}
		fmt.Printf("- %s (%s): %s\n", issue.References.Short, faint.Sprint(issue.Title), severity)
	}
	fmt.Println()
	color.Green("Commands:")
	for _, command := range commands {
		fmt.Println(command)
	}
}

func matchIssueToRule(issue *gitlab.Issue) *parser.RuleDef {
	var existingPath string

	parts := strings.Split(issue.Title, " ")
	for _, part := range parts {
		if strings.Contains(part, ".yml") {
			existingPath = part
		}
	}

	path := ""

	for _, rulePath := range existingRules {
		a := strings.ReplaceAll(rulePath, "/", " ")
		a = strings.ReplaceAll(a, "-", " ")
		a = strings.ReplaceAll(a, "_", " ")
		existingPath = strings.ReplaceAll(existingPath, "/", " ")
		existingPath = strings.ReplaceAll(existingPath, "\\", " ")
		existingPath = strings.ReplaceAll(existingPath, "-", " ")
		existingPath = strings.ReplaceAll(existingPath, "_", " ")

		if strings.Contains(a, existingPath) {
			path = rulePath
		}
	}

	if path == "" {
		fmt.Println(prefix, "no rule found:", issue.Title)
		return nil
	}

	f, err := os.Open(path)
	if err != nil {
		fmt.Println(prefix, err)
		os.Exit(1)
	}
	defer f.Close()
	rule, err := parser.ParseRuleDef(f)
	if err != nil {
		fmt.Println(prefix, "parsing rule def:", err)
		os.Exit(1)
	}
	return &rule
}

func listExistingRules() []string {
	out, err := exec.Command("find", "sast-rules", "-name", "*.yml").CombinedOutput()

	if err != nil {
		panic(fmt.Errorf("running find: %w\n%s", err, out))
	}

	return strings.Split(string(out), "\n")
}

func fetchAllIssues(client *gitlab.Client) chan *gitlab.Issue {
	ch := make(chan *gitlab.Issue)
	go func() {
		for page := 1; ; page++ {
			issues, _, err := client.Issues.ListProjectIssues("gitlab-org/gitlab", &gitlab.ListProjectIssuesOptions{
				ListOptions: gitlab.ListOptions{
					Page: page,
				},
				Labels:    &gitlab.LabelOptions{"Category:SAST", "SAST::Ruleset::Enhancement"},
				NotLabels: &gitlab.LabelOptions{"SAST::Ruleset::P1", "SAST::Ruleset::P2", "SAST::Ruleset::P3", "SAST::Ruleset::P4"},
				State:     gitlab.Ptr("opened"),
				OrderBy:   gitlab.Ptr("created_at"),
			})
			if err != nil {
				fmt.Println(prefix, err)
				os.Exit(1)
			}
			if len(issues) == 0 {
				close(ch)
			}
			for _, issue := range issues {
				ch <- issue
			}
		}
	}()
	return ch
}
