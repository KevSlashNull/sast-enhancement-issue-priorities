package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"semgrep-enhance-rules/internal/parser"
	"strings"

	"gopkg.in/yaml.v3"
)

var existingRules []string

func listRules() []string {
	out, err := exec.Command("find", "semgrep-rules", "-name", "*.yaml").CombinedOutput()

	if err != nil {
		panic(fmt.Errorf("running find: %w\n%s", err, out))
	}

	return strings.Split(string(out), "\n")
}

func main() {
	existingRules = listRules()
	data, err := os.ReadFile("issues.txt")

	if err != nil {
		panic(err)
	}

	lines := strings.Split(string(data), "\n")

	csvRules, err := os.OpenFile("semgrep-rules.csv", os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		panic(err)
	}
	defer csvRules.Close()

	setPrioritiesScript, err := os.OpenFile("set-priorities.sh", os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		panic(err)
	}
	defer setPrioritiesScript.Close()

	fmt.Fprintln(csvRules, "iid,rule_path,severity,label")
	fmt.Fprintln(setPrioritiesScript, "#!/bin/sh")

	for _, line := range lines {
		if line == "" {
			continue
		}
		parts := strings.Split(line, "\t")

		id := parts[0][1:]
		title := parts[2]
		labels := parts[3]

		const prefix = "Embed Semgrep Community Rule "
		if !strings.HasPrefix(title, prefix) {
			continue
		}
		title, _ = strings.CutPrefix(title, prefix)

		rule, path := resolveRule(title)

		label := rule.ToPriorityLabel()

		fmt.Fprintf(csvRules, "%s,%s,%s,%s\n", id, path, rule.Severity(), label)
		if !strings.Contains(labels, "priority") {
			fmt.Fprintf(setPrioritiesScript, "glab issue update %s -R gitlab-org/gitlab --label %s\n", id, label)
		}
	}
}

func resolveRule(name string) (ruleDef parser.RuleDef, path string) {
	parts := strings.Split(name, ".")
	parts = parts[:len(parts)-1]
	name = strings.Join(parts, ".")

	path = strings.ReplaceAll(name, ".", "/") + ".yaml"
	path = filepath.Join("semgrep-rules", path)

	for _, rule := range existingRules {
		if strings.EqualFold(rule, path) {
			path = rule
		}
	}

	b, err := os.ReadFile(path)
	if err != nil {
		panic(fmt.Errorf("resolving %s: %w", name, err))
	}

	err = yaml.Unmarshal(b, &ruleDef)
	if err != nil {
		panic(fmt.Errorf("unmarshaling %s: %w", name, err))
	}

	return
}
